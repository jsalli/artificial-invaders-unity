# Install environment
- This repo is tested with python 3.6.8 and Unity 2019.2.0f1 on Ubuntu 18.04.

- Install Unity ml-agent. See the instructions from their repo
https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Installation.md

- Download this repo

## Using an already trained brain to play the game
- Select the Academy GameObject from the left side Hierarchy-menu.
- Uncheck the "Control"-checkbox on the right side Inspector-menu.
- Find the brain "ArtificialInvaderLearning".
- You can tune the game with the "Reset parameters"

![alt text](./docs/images/unity_academy_setup.png "Academy options")

- Drag a trained neural network to the brain's "Model"-field.

![alt text](./docs/images/brain_setup.png "Brain options")

- Click the Play button

## Training a new neural network
- Select the Academy GameObject from the left side Hierarchy-menu.
- Check the "Control"-checkbox on the right side Inspector-menu.
- Open a terminal window and go to the root folder of this project.
- Run the following command.
  ``` sh
  mlagents-learn config/trainer_config.yaml --run-id=artificialInvader1.0 --train --save-freq=10000 --curriculum=config/curricula/artificial-invader/
  ```
In the above command set the `--run-id` to a value which you want the name of the model to be.
If you want to continue a previous training and `--load` to the command and make sure `--run-id` references to an existing model.

- The Python program should say that it's waiting you to press Play button.
- Go to Unity and press play button.
- After training a new model is created to the "models"-folder in the root-folder.
- You can edit the `config/curricula/artificial-invader/ArtificialInvaderLearning.json` file to tune the game levels for training

