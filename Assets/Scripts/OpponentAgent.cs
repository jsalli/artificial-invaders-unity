using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentAgent : MonoBehaviour
{

    public GameObject pointA;
    public GameObject pointB;
    public float step = 0.1f;

    private Vector3 destination;
    private Vector3 currentPos;
    private Vector3 speed;
    private Vector3 oldPosition;

    private void Awake()
    {
        Reset();
        oldPosition = currentPos;
    }

    private void Update() 
    {
        currentPos = Vector3.MoveTowards(currentPos, destination, step);
        transform.localPosition = currentPos;
        speed = (currentPos - oldPosition) / Time.deltaTime;

        if (currentPos == destination) {
            if (destination == pointA.transform.localPosition) destination = pointB.transform.localPosition;
            else destination = pointA.transform.localPosition;
        }
        oldPosition = currentPos;
    }

    public void Reset()
    {
        currentPos = pointB.transform.localPosition;
        destination = pointA.transform.localPosition;
    }

    public Vector3 GetSpeed()
    {
        return speed;
    }
}