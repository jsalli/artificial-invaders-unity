﻿//Put this script on your blue cube.
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class PushAgentBasic : Agent
{
    public bool resetFromNegativeReward;
    public GameObject goodBallPrefab;
    public GameObject badBallPrefab;
    public List<GameObject> levels = new List<GameObject>();
    /// <summary>
    /// The ground. The bounds are used to spawn the elements.
    /// </summary>
	private GameObject ground;

    public GameObject area;

    /// <summary>
    /// The area bounds.
    /// </summary>
	[HideInInspector]
    public Bounds areaBounds;

    PushBlockAcademy academy;

    /// <summary>
    /// The goal to push the block to.
    /// </summary>
    //public GameObject goal;

    /// <summary>
    /// Detects when the block touches the goal.
    /// </summary>
	[HideInInspector]
    public GoalDetect goalDetect;

    public bool useVectorObs;

    Rigidbody blockRB;  //cached on initialization
    Rigidbody agentRB;  //cached on initialization
    Material groundMaterial; //cached on Awake()
    RayPerception rayPer;
    
    // float[] rayAngles = { 0f, 45f, 90f, 135f, 180f, 110f, 70f };'
    float[] rayAngles = { -90f, -45f, -20f, 0f, 20f, 45f, 90f };
    string[] detectableObjects = { "homegoal", "opponentgoal", "wall", "opponentagent" };

    /// <summary>
    /// We will be changing the ground material based on success/failue
    /// </summary>
    Renderer groundRenderer;

    private int currentLevel = -1;

    private InvaderRayPerception invaderRayPerception;

    private List<GameObject> goodBalls = new List<GameObject>();
    private List<GameObject> badBalls = new List<GameObject>();

    // private float totalReward = 0.0f;
    private float timeStepRewardMultiplier = 1.0f;

    void Awake()
    {
        academy = FindObjectOfType<PushBlockAcademy>(); //cache the academy 
    }

    public override void InitializeAgent()
    {
        base.InitializeAgent();
        rayPer = GetComponent<RayPerception>();
        invaderRayPerception = GetComponent<InvaderRayPerception>();

        // Cache the agent rigidbody
        agentRB = GetComponent<Rigidbody>();

        SetResetParameters();
    }

    private void CreateBalls()
    {
        var resetParams = academy.resetParameters;
        int numberGoodBalls = (int) resetParams["number_of_good_balls"];
        int numberBadBalls = (int) resetParams["number_of_bad_balls"];
        int randomizeBallType = (int) resetParams["randomize_ball_type"];

        DeleteBalls(goodBalls);
        DeleteBalls(badBalls);

        if (randomizeBallType > 0) {
            int selection = UnityEngine.Random.Range(0, 2);
            if (selection == 0) numberBadBalls = 0;
            else numberGoodBalls = 0;
        }

        InstantiateBalls(goodBallPrefab, goodBalls, numberGoodBalls);
        InstantiateBalls(badBallPrefab, badBalls, numberBadBalls);
    }

    private void DeleteBalls(List<GameObject> ballsList)
    {
        if(ballsList.Count > 0)
        {
            ballsList.ForEach(obj => { Destroy(obj.gameObject); });
            ballsList.Clear();
        }
    }

    private void InstantiateBalls(GameObject ballPrefab, List<GameObject> ballsList, int amount)
    {
        for (int i = 0; i < amount; ++i)
        {
            GameObject newObj = Instantiate(ballPrefab, GetRandomSpawnPos(), Quaternion.identity, area.transform);
            GoalDetect goalDetect = newObj.GetComponent<GoalDetect>();
            goalDetect.agent = this;
            ballsList.Add(newObj);
        }
    }

    public override void CollectObservations()
    {
        if (useVectorObs)
        {
            var rayDistance = 40.0f;//56.57f; // SQRT(40**2 + 40**2) = 56.57
            
            AddVectorObs(rayPer.Perceive(rayDistance, rayAngles, detectableObjects, 1.5f, 0f));
            AddVectorObs(invaderRayPerception.Perceive(rayDistance, rayAngles, new List<GameObject>[] {badBalls, goodBalls}));
        }
    }

    /// <summary>
    /// Use the ground's bounds to pick a random spawn position.
    /// </summary>
    public Vector3 GetRandomSpawnPos()
    {
        var resetParams = academy.resetParameters;

        int maxTries = 500;
        int currentTry = 0;
        bool foundNewSpawnLocation = false;
        Vector3 randomSpawnPos = Vector3.zero;
        while (foundNewSpawnLocation == false && currentTry < maxTries)
        {
            float randomPosX = UnityEngine.Random.Range(-areaBounds.extents.x * academy.spawnAreaMarginMultiplier,
                                areaBounds.extents.x * academy.spawnAreaMarginMultiplier);

            float randomPosZ = UnityEngine.Random.Range(-areaBounds.extents.z * academy.spawnAreaMarginMultiplier,
                                            areaBounds.extents.z * academy.spawnAreaMarginMultiplier);
            randomSpawnPos = ground.transform.position + new Vector3(randomPosX, 1f, randomPosZ);
            if (Physics.CheckBox(randomSpawnPos, new Vector3(resetParams["ball_scale"] + 0.3f, 0.01f, resetParams["ball_scale"] + 0.3f)) == false)
            {
                foundNewSpawnLocation = true;
            }
            currentTry++;
        }
        if (foundNewSpawnLocation) return randomSpawnPos;
        
        Debug.Log("Could not find a place for the object.");
        throw new Exception();
    }

    /// <summary>
    /// Called when the agent moves the block into the goal.
    /// </summary>
    public void IScoredAGoal(bool opponentGoal, bool goodBall, GameObject ball)
    {
        DestroyBall(goodBall, ball);

        float reward = CalculateReward(opponentGoal, goodBall);
        AddReward(reward);

        Material mat =  null;
        if (reward > 0)
        {
            mat = academy.rightGoalScoredMaterial;
        }
        else
        {
            mat = academy.wrongGoalScoredMaterial;
        }

        // Swap ground material for a bit to indicate we scored.
        StartCoroutine(GoalScoredSwapGroundMaterial(mat, 0.5f));

        IsGameFinished(reward);
    }

    private void DestroyBall(bool goodBall, GameObject ball)
    {
        if(goodBall)
        {
            goodBalls.Remove(ball);
        }
        else
        {
            badBalls.Remove(ball);
        }
        Destroy(ball.gameObject);
    }

    private float CalculateReward(bool opponentGoal, bool goodBall)
    {
        var resetParams = academy.resetParameters;

        float goodBallWrongGoalReward = resetParams["good_ball_wrong_goal_reward"];
        float goodBallRightGoalReward = resetParams["good_ball_right_goal_reward"];
        float badBallWrongGoalReward = resetParams["bad_ball_wrong_goal_reward"];
        float badBallRightGoalReward = resetParams["bad_ball_right_goal_reward"];

        float reward = 0.0f;
        if (goodBall && opponentGoal) reward = goodBallWrongGoalReward;
        else if (goodBall && !opponentGoal) reward = goodBallRightGoalReward;
        else if (!goodBall && !opponentGoal) reward = badBallWrongGoalReward;
        else if (!goodBall && opponentGoal) reward = badBallRightGoalReward;
        return reward;
    }

    private void IsGameFinished(float reward)
    {
        if (resetFromNegativeReward && reward < 0.0f)
        {
            Done();
        }
        else if (goodBalls.Count == 0 && badBalls.Count == 0)
        {
            Done();
        }
    }

    /// <summary>
    /// Swap ground material, wait time seconds, then swap back to the regular material.
    /// </summary>
    IEnumerator GoalScoredSwapGroundMaterial(Material mat, float time)
    {
        groundRenderer.material = mat;
        yield return new WaitForSeconds(time); // Wait for 2 sec
        groundRenderer.material = groundMaterial;
    }

    /// <summary>
    /// Moves the agent according to the selected action.
    /// </summary>
	public void MoveAgent(float[] act)
    {

        Vector3 dirToGo = Vector3.zero;
        Vector3 rotateDir = Vector3.zero;

        int action = Mathf.FloorToInt(act[0]);

        // Goalies and Strikers have slightly different action spaces.
        switch (action)
        {
            case 1:
                dirToGo = transform.forward * 1f;
                break;
            case 2:
                dirToGo = transform.forward * -1f;
                break;
            case 3:
                rotateDir = transform.up * 1f;
                break;
            case 4:
                rotateDir = transform.up * -1f;
                break;
        }
        transform.Rotate(rotateDir, Time.fixedDeltaTime * 200f);
        agentRB.AddForce(dirToGo * academy.agentRunSpeed,
                         ForceMode.VelocityChange);
    }

    /// <summary>
    /// Called every step of the engine. Here the agent takes an action.
    /// </summary>
	public override void AgentAction(float[] vectorAction, string textAction)
    {
        // Move the agent using the action.
        MoveAgent(vectorAction);

        // Penalty given each step to encourage agent to finish task quickly.
        AddReward((-1f / agentParameters.maxStep) * timeStepRewardMultiplier);
    }


    /// <summary>
    /// In the editor, if "Reset On Done" is checked then AgentReset() will be 
    /// called automatically anytime we mark done = true in an agent script.
    /// </summary>
	public override void AgentReset()
    {
        // totalReward = 0.0f;
        int rotation = UnityEngine.Random.Range(0, 4);
        float rotationAngle = rotation * 90f;
        area.transform.Rotate(new Vector3(0f, rotationAngle, 0f));

        // ResetBalls();
        CreateBalls();
        transform.position = GetRandomSpawnPos();
        agentRB.velocity = Vector3.zero;
        agentRB.angularVelocity = Vector3.zero;

        SetResetParameters();
    }

    public void SetGroundMaterialFriction()
    {
        var resetParams = academy.resetParameters;

        var groundCollider = ground.GetComponent<Collider>() as Collider;

        groundCollider.material.dynamicFriction = resetParams["dynamic_friction"];
        groundCollider.material.staticFriction = resetParams["static_friction"];
    }

    public void SetBallsProperties()
    {
        var resetParams = academy.resetParameters;
        foreach(List<GameObject> ballList in new List<GameObject>[] {goodBalls, badBalls})
        {
            foreach(GameObject ball in ballList)
            {
                // ball.transform.localScale = new Vector3(resetParams["ball_scale"], resetParams["ball_scale"], resetParams["ball_scale"]);
                ball.transform.localScale = new Vector3(resetParams["ball_scale"], 0.75f, resetParams["ball_scale"]);

                ball.GetComponent<Rigidbody>().drag = resetParams["ball_drag"];
            }
        }
    }

    public void SetResetParameters()
    {
        SetLevel();
        SetGroundMaterialFriction();
        SetBallsProperties();
    }

    private void SetLevel()
    {
        var resetParams = academy.resetParameters;
        var newLevel = (int) resetParams["level_number"];

        if (currentLevel == newLevel) return;
        currentLevel = newLevel;
        
        for (int i = 0; i < levels.Count; ++i)
        {
            if (i != currentLevel) levels[i].SetActive(false);
            else levels[i].SetActive(true);
        }
        
        ground = levels[currentLevel].transform.Find("Ground").gameObject; //GameObject.FindWithTag("ground");

        // // Get the ground's bounds
        areaBounds = ground.GetComponent<Collider>().bounds;
        // // Get the ground renderer so we can change the material when a goal is scored
        groundRenderer = ground.GetComponent<Renderer>();
        // // Starting material
        groundMaterial = groundRenderer.material;
        agentParameters.maxStep = (int) resetParams["agent_max_steps"];
        timeStepRewardMultiplier = resetParams["time_step_reward_multiplier"];

        CreateBalls();
    }
}
